/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.vico.web.rest.vm;
