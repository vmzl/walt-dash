package fr.vico.web.rest;

import fr.vico.domain.Pair;
import fr.vico.service.PairService;
import fr.vico.service.WaltWalletService;
import fr.vico.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.vico.domain.Pair}.
 */
@RestController
@RequestMapping("/api")
public class PairResource {

    private final Logger log = LoggerFactory.getLogger(PairResource.class);

    private static final String ENTITY_NAME = "waltPairPair";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PairService pairService;
    private final WaltWalletService waltWalletService;

    public PairResource(PairService pairService, WaltWalletService waltWalletService) {
        this.pairService = pairService;
        this.waltWalletService = waltWalletService;
    }

    /**
     * {@code POST  /pairs} : Create a new pair.
     *
     * @param pair the pair to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pair, or with status {@code 400 (Bad Request)} if the pair has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pairs")
    public ResponseEntity<Pair> createPair(@RequestBody Pair pair) throws URISyntaxException {
        log.debug("REST request to save Pair : {}", pair);
        if (pair.getId() != null) {
            throw new BadRequestAlertException("A new pair cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Pair result = pairService.save(pair);
        return ResponseEntity.created(new URI("/api/pairs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pairs} : Updates an existing pair.
     *
     * @param pair the pair to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pair,
     * or with status {@code 400 (Bad Request)} if the pair is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pair couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pairs")
    public ResponseEntity<Pair> updatePair(@RequestBody Pair pair) throws URISyntaxException {
        log.debug("REST request to update Pair : {}", pair);
        if (pair.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Pair result = pairService.save(pair);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pair.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /pairs} : get all the pairs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pairs in body.
     */
    @GetMapping("/pairs")
    public List<Pair> getAllPairs() {
        log.debug("REST request to get all Pairs");
        return pairService.findAll();
    }

    /**
     * {@code GET  /pairs/:id} : get the "id" pair.
     *
     * @param id the id of the pair to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pair, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pairs/{id}")
    public ResponseEntity<Pair> getPair(@PathVariable Long id) {
        log.debug("REST request to get Pair : {}", id);
        Optional<Pair> pair = pairService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pair);
    }

    /**
     * {@code GET  /pairs/:id} : get the "id" pair.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pairs in body.
     */
    @GetMapping("/pairs/create")
    public List<Pair> createPairs() {
        log.debug("REST request to create Pairs");

        return waltWalletService.createOrUpdatePairs();
    }

    /**
     * {@code GET  /pairs/:pairName} : get the "id" pair.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the pair with price updated in body.
     */
    @GetMapping("/pairs/updatePrices/{pairName}")
    public Pair updatePairPrice(@PathVariable String pairName) {
        log.debug("REST request to update Pair price : {}", pairName);
        Pair pair = pairService.getByName(pairName).get(); //If Pair with this pairName not found, will throw a NoSuchElementException
        return pairService.updatePrice(pair);
    }


    @GetMapping("/pairs/updatePrices")
    public List<Pair> updatePairsPrice() {
        log.debug("REST request to update Pairs price");

        return pairService.updatePrices();
    }

    @GetMapping("/pairs/symbol/{symbol}")
    public List<Pair> getPairWithSymbol(@PathVariable String symbol) {
        log.debug("REST request to get all Pair with symbol : {}", symbol);

        List<Pair> pairs = pairService.findAll();
        pairs.removeIf(pair -> !pair.getSymbol().getSymbol().equals(symbol));
        log.debug("Found {} matching pairs for symbol {}", pairs.size(), symbol);
        return pairs;
    }

    /**
     * {@code DELETE  /pairs/:id} : delete the "id" pair.
     *
     * @param id the id of the pair to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pairs/{id}")
    public ResponseEntity<Void> deletePair(@PathVariable Long id) {
        log.debug("REST request to delete Pair : {}", id);
        pairService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
