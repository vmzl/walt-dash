package fr.vico.web.rest;

import fr.vico.domain.Symbol;
import fr.vico.service.SymbolService;
import fr.vico.service.WaltWalletService;
import fr.vico.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.vico.domain.Symbol}.
 */
@RestController
@RequestMapping("/api")
public class SymbolResource {

    private final Logger log = LoggerFactory.getLogger(SymbolResource.class);

    private static final String ENTITY_NAME = "waltPairSymbol";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SymbolService symbolService;
    private final WaltWalletService waltWalletService;

    public SymbolResource(SymbolService symbolService, WaltWalletService waltWalletService) {
        this.symbolService = symbolService;
        this.waltWalletService = waltWalletService;
    }

    /**
     * {@code POST  /symbols} : Create a new symbol.
     *
     * @param symbol the symbol to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new symbol, or with status {@code 400 (Bad Request)} if the symbol has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/symbols")
    public ResponseEntity<Symbol> createSymbol(@Valid @RequestBody Symbol symbol) throws URISyntaxException {
        log.debug("REST request to save Symbol : {}", symbol);
        if (symbol.getId() != null) {
            throw new BadRequestAlertException("A new symbol cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Symbol result = symbolService.save(symbol);
        return ResponseEntity.created(new URI("/api/symbols/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /symbols} : Updates an existing symbol.
     *
     * @param symbol the symbol to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated symbol,
     * or with status {@code 400 (Bad Request)} if the symbol is not valid,
     * or with status {@code 500 (Internal Server Error)} if the symbol couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/symbols")
    public ResponseEntity<Symbol> updateSymbol(@Valid @RequestBody Symbol symbol) throws URISyntaxException {
        log.debug("REST request to update Symbol : {}", symbol);
        if (symbol.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Symbol result = symbolService.save(symbol);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, symbol.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /symbols} : get all the symbols.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of symbols in body.
     */
    @GetMapping("/symbols")
    public List<Symbol> getAllSymbols() {
        log.debug("REST request to get all Symbols");
        return symbolService.findAll();
    }

    /**
     * {@code GET  /symbols/load} : load symbols from waltwallet microservices.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of symbols loaded in body.
     */
    @GetMapping("/symbols/load")
    public List<Symbol> LoadSymbols() {
        log.debug("REST request to import all Symbols from WaltWallet");
        List<Symbol> symbols =  waltWalletService.importSymbolsFromWaltWallet();
        for(Symbol symbol : symbols){
            symbol = symbolService.save(symbol);
        }
        return symbols;
    }

    /**
     * {@code GET  /symbols/:id} : get the "id" symbol.
     *
     * @param id the id of the symbol to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the symbol, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/symbols/{id}")
    public ResponseEntity<Symbol> getSymbol(@PathVariable Long id) {
        log.debug("REST request to get Symbol : {}", id);
        Optional<Symbol> symbol = symbolService.findOne(id);
        return ResponseUtil.wrapOrNotFound(symbol);
    }

    /**
     * {@code DELETE  /symbols/:id} : delete the "id" symbol.
     *
     * @param id the id of the symbol to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/symbols/{id}")
    public ResponseEntity<Void> deleteSymbol(@PathVariable Long id) {
        log.debug("REST request to delete Symbol : {}", id);
        symbolService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
