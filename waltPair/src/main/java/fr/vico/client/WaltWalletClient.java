package fr.vico.client;

import feign.RequestLine;
import fr.vico.domain.Symbol;

import java.util.List;

public interface WaltWalletClient {

    @RequestLine("GET /cryptos")
    List<Symbol> findAllCryptos();
}
