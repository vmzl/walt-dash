package fr.vico.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Quote.
 */
@Entity
@Table(name = "quote")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Quote implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "symbol", nullable = false, unique = true)
    private String symbol;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "price_usdt")
    private Float priceUSDT;

    public Quote() {
    }

    public Quote(@NotNull String symbol, @NotNull String name, Float priceUSDT) {
        this.symbol = symbol;
        this.name = name;
        this.priceUSDT = priceUSDT;
    }

    public Quote(Long id, @NotNull String symbol, @NotNull String name, Float priceUSDT) {
        this.id = id;
        this.symbol = symbol;
        this.name = name;
        this.priceUSDT = priceUSDT;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public Quote symbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public Quote name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPriceUSDT() {
        return priceUSDT;
    }

    public Quote priceUSDT(Float priceUSDT) {
        this.priceUSDT = priceUSDT;
        return this;
    }

    public void setPriceUSDT(Float priceUSDT) {
        this.priceUSDT = priceUSDT;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Quote)) {
            return false;
        }
        return id != null && id.equals(((Quote) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Quote{" +
            "id=" + getId() +
            ", symbol='" + getSymbol() + "'" +
            ", name='" + getName() + "'" +
            ", priceUSDT=" + getPriceUSDT() +
            "}";
    }
}
