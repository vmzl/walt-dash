package fr.vico.repository;

import fr.vico.domain.Pair;

import fr.vico.domain.Quote;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the Pair entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PairRepository extends JpaRepository<Pair, Long> {

    /**
     * This method is created by JPA from its name.
     * @param name
     * @return Pair with the name, null instead
     */
    Optional<Pair> getByName(String name);
}
