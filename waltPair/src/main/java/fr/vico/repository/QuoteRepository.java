package fr.vico.repository;

import fr.vico.domain.Quote;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the Quote entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuoteRepository extends JpaRepository<Quote, Long> {

    /**
     * This method is created by JPA from its name.
     * @param symbol
     * @return Quote with the symbol, null instead
     */
    Optional<Quote> getBySymbol(String symbol);
}
