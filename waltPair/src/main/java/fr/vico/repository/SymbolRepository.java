package fr.vico.repository;

import fr.vico.domain.Quote;
import fr.vico.domain.Symbol;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Symbol entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SymbolRepository extends JpaRepository<Symbol, Long> {
    /**
     * This method is created by JPA from its name.
     * @param symbol
     * @return Crypto with the symbol, null instead
     */
    Symbol getBySymbol(String symbol);
}
