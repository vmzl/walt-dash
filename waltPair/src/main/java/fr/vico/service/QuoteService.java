package fr.vico.service;

import fr.vico.domain.Pair;
import fr.vico.domain.Quote;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Quote}.
 */
public interface QuoteService {

    /**
     * Save a quote.
     *
     * @param quote the entity to save.
     * @return the persisted entity.
     */
    Quote save(Quote quote);

    /**
     * Get all the quotes.
     *
     * @return the list of entities.
     */
    List<Quote> findAll();


    /**
     * Get the "id" quote.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Quote> findOne(Long id);

    Optional<Quote> getBySymbol(String symbol);

    /**
     * Delete the "id" quote.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Save all the quotes
     * @param quotes
     * @return the list of saved entities
     */
    List<Quote> saveAll(List<Quote> quotes);

    Quote updatePrice(Quote quote);

    List<Quote> updatePrices();
}
