package fr.vico.service;

import fr.vico.domain.Pair;
import fr.vico.domain.Quote;

public interface ExchangeAPIService {

    public Quote updateQuotePrice(Quote quote);

    public Pair updatePairPrice(Pair pair);

}
