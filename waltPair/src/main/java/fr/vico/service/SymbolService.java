package fr.vico.service;

import fr.vico.domain.Quote;
import fr.vico.domain.Symbol;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Symbol}.
 */
public interface SymbolService {

    /**
     * Save a symbol.
     *
     * @param symbol the entity to save.
     * @return the persisted entity.
     */
    Symbol save(Symbol symbol);

    /**
     * Get all the symbols.
     *
     * @return the list of entities.
     */
    List<Symbol> findAll();


    /**
     * Get the "id" symbol.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Symbol> findOne(Long id);

    Symbol getBySymbol(String symbol);

    /**
     * Delete the "id" symbol.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
