package fr.vico.service;

import fr.vico.domain.Pair;
import fr.vico.domain.Quote;
import fr.vico.domain.Symbol;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Pair}.
 */
public interface PairService {

    /**
     * Save a pair.
     *
     * @param pair the entity to save.
     * @return the persisted entity.
     */
    Pair save(Pair pair);

    /**
     * Get all the pairs.
     *
     * @return the list of entities.
     */
    List<Pair> findAll();


    /**
     * Get the "id" pair.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Pair> findOne(Long id);

    Optional<Pair> getByName(String name);

    /**
     * Delete the "id" pair.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);


    /**
     * Create and save all Pairs from "symbols" and "quotes"
     * @param symbols
     * @param quotes
     * @return the entities
     */
    List<Pair> createPairs(List<Symbol> symbols, List<Quote> quotes);

    /**
     * Save all the pairs
     * @param pairs
     * @return the list of saved entities
     */
    List<Pair> saveAll(List<Pair> pairs);

    Pair updatePrice(Pair pair);

    List<Pair> updatePrices();
}
