package fr.vico.service.impl;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.TickerStatistics;
import com.binance.api.client.exception.BinanceApiException;
import fr.vico.domain.Pair;
import fr.vico.domain.Quote;
import fr.vico.service.ExchangeAPIService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class BinanceAPIServiceImpl implements ExchangeAPIService {

    private final Logger log = LoggerFactory.getLogger(BinanceAPIServiceImpl.class);

    private BinanceApiRestClient client;

    public BinanceAPIServiceImpl() {
        BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("Q0HfKdFIZlHLYF2hddCufk7lqlZEg8Sz1zeWjH3bYHftNgcaTcZrpgeatewNUUF5", "6UAGjA3fypSkUqSjg17YlYpTE0ac6YW38saUSYSVYXGfz7Z1INF49hHyGi18pcLV");
        this.client = factory.newRestClient();
    }

    @Override
    public Quote updateQuotePrice(Quote quote) {
        try {
            switch (quote.getSymbol()){
                case "USDT":
                    quote.setPriceUSDT(1F);
                    break;
                case "BTC":
                    TickerStatistics tickerStatistics = client.get24HrPriceStatistics("BTCUSDT");
                    quote.setPriceUSDT(Float.parseFloat(tickerStatistics.getLastPrice()));
                    break;
                default:
                    log.debug("Unknow Quote : {}", quote.getSymbol());
            }
        }catch(BinanceApiException e){
            log.debug("This quote get an error : {} Quote will be deleted.", e.getMessage());
            quote=null;
        }
        return quote;

    }

    @Override
    public Pair updatePairPrice(Pair pair) {

        log.debug("Looking for price of Pair : {}", pair.getName());
        try {
            TickerStatistics tickerStatistics = client.get24HrPriceStatistics(pair.getName());
            pair.setPrice(Float.parseFloat(tickerStatistics.getLastPrice()));
        }catch(BinanceApiException e){
            log.debug("This pair get an error : {} Pair will be deleted if not USDT.", e.getMessage());
            pair=null;
        }

        return pair;
    }
}
