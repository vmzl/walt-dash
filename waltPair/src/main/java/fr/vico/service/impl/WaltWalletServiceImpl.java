package fr.vico.service.impl;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.slf4j.Slf4jLogger;
import fr.vico.client.WaltWalletClient;
import fr.vico.client.UserFeignClientInterceptor;
import fr.vico.domain.Pair;
import fr.vico.domain.Quote;
import fr.vico.domain.Symbol;
import fr.vico.service.PairService;
import fr.vico.service.QuoteService;
import fr.vico.service.SymbolService;
import fr.vico.service.WaltWalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class WaltWalletServiceImpl implements WaltWalletService {
    private static final String WALT_WALLET_URL = "http://localhost:8310/services/waltwallet/api";

    private final Logger log = LoggerFactory.getLogger(WaltWalletServiceImpl.class);

    private final PairService pairService;
    private final QuoteService quoteService;
    private final SymbolService symbolService;


    public WaltWalletServiceImpl(PairService pairService, QuoteService quoteService, SymbolService symbolService) {
        this.pairService = pairService;
        this.quoteService = quoteService;
        this.symbolService = symbolService;
    }


    @Override
    public List<Symbol> importSymbolsFromWaltWallet() {

        GsonDecoder gsonDecoder = new GsonDecoder();

        WaltWalletClient waltWalletClient = Feign.builder()
            //.client(new OkHttpClient()) //I let both because they can be usefull for others feignclient fonctions ...?
            //.encoder(new GsonEncoder())
            .decoder(new GsonDecoder())
            .logger(new Slf4jLogger(WaltWalletClient.class))
            .logLevel(feign.Logger.Level.FULL)
            .requestInterceptor(new UserFeignClientInterceptor())
            .target(WaltWalletClient.class, WALT_WALLET_URL);

        List<Symbol> symbols = waltWalletClient.findAllCryptos();
        log.debug("Try to import {} symbols", symbols.size());

        return symbols;
    }

    @Override
    public List<Pair> createOrUpdatePairs() {
        //importSymbolsFromWaltWallet();
        List<Quote> quotes = new ArrayList<>();
        quotes.add(new Quote(1L,"USDT", "USDT", 1F));
        quotes.add(new Quote(2L, "BTC", "Bitcoin", 0F));

        List<Symbol> symbols = symbolService.findAll();
        quotes = quoteService.saveAll(quotes);
        return pairService.createPairs(symbols, quotes);
    }


}
