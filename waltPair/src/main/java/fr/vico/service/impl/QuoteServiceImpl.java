package fr.vico.service.impl;

import fr.vico.domain.Pair;
import fr.vico.service.ExchangeAPIService;
import fr.vico.service.QuoteService;
import fr.vico.domain.Quote;
import fr.vico.repository.QuoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Quote}.
 */
@Service
@Transactional
public class QuoteServiceImpl implements QuoteService {

    private final Logger log = LoggerFactory.getLogger(QuoteServiceImpl.class);

    private final QuoteRepository quoteRepository;
    private final ExchangeAPIService exchangeAPIService;

    public QuoteServiceImpl(QuoteRepository quoteRepository, ExchangeAPIService exchangeAPIService) {
        this.quoteRepository = quoteRepository;
        this.exchangeAPIService = exchangeAPIService;
    }

    @Override
    public Quote save(Quote quote) {
        log.debug("Request to save Quote : {}", quote);
        Optional<Quote> existQuote = getBySymbol(quote.getSymbol());
        if(existQuote.isPresent()) {
            quote.setId(existQuote.get().getId());
        }
        return quoteRepository.save(quote);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Quote> findAll() {
        log.debug("Request to get all Quotes");
        return quoteRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Quote> findOne(Long id) {
        log.debug("Request to get Quote : {}", id);
        return quoteRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Quote : {}", id);
        quoteRepository.deleteById(id);
    }

    @Override
    public Optional<Quote> getBySymbol(String symbol) {
        log.debug("Request to get Quote with symbol : {}", symbol);
        return quoteRepository.getBySymbol(symbol);
    }

    @Override
    public List<Quote> saveAll(List<Quote> quotes) {
        for(Quote quote : quotes){
            quote = save(quote);
        }
        return quotes;
    }

    @Override
    public Quote updatePrice(Quote quote) {
        Quote updatedPriceQuote = exchangeAPIService.updateQuotePrice(quote);
        if(updatedPriceQuote == null){
            delete(quote.getId());
        }else{
            updatedPriceQuote = save(updatedPriceQuote);
        }
        return updatedPriceQuote;
    }

    @Override
    public List<Quote> updatePrices() {
        List<Quote> quotes = findAll();
        for(Quote quote : quotes){
            quote = updatePrice(quote);
        }
        return quotes;
    }
}
