package fr.vico.service.impl;

import fr.vico.domain.Quote;
import fr.vico.service.SymbolService;
import fr.vico.domain.Symbol;
import fr.vico.repository.SymbolRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Symbol}.
 */
@Service
@Transactional
public class SymbolServiceImpl implements SymbolService {

    private final Logger log = LoggerFactory.getLogger(SymbolServiceImpl.class);

    private final SymbolRepository symbolRepository;

    public SymbolServiceImpl(SymbolRepository symbolRepository) {
        this.symbolRepository = symbolRepository;
    }

    @Override
    public Symbol save(Symbol symbol) {
        log.debug("Request to save Symbol : {}", symbol);
        Symbol existSymbol = getBySymbol(symbol.getSymbol());
        if(existSymbol!=null) {
            symbol = existSymbol;
        }
        return symbolRepository.save(symbol);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Symbol> findAll() {
        log.debug("Request to get all Symbols");
        return symbolRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Symbol> findOne(Long id) {
        log.debug("Request to get Symbol : {}", id);
        return symbolRepository.findById(id);
    }

    @Override
    public Symbol getBySymbol(String symbol) {
        log.debug("Request to get Crypto with symbol : {}", symbol);
        return symbolRepository.getBySymbol(symbol);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Symbol : {}", id);
        symbolRepository.deleteById(id);
    }
}
