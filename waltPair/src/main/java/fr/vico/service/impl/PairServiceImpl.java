package fr.vico.service.impl;

import fr.vico.domain.Quote;
import fr.vico.domain.Symbol;
import fr.vico.service.ExchangeAPIService;
import fr.vico.service.PairService;
import fr.vico.domain.Pair;
import fr.vico.repository.PairRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Pair}.
 */
@Service
@Transactional
public class PairServiceImpl implements PairService {

    private final Logger log = LoggerFactory.getLogger(PairServiceImpl.class);

    private final PairRepository pairRepository;
    private final ExchangeAPIService exchangeAPIService;

    public PairServiceImpl(PairRepository pairRepository, ExchangeAPIService exchangeAPIService) {
        this.pairRepository = pairRepository;
        this.exchangeAPIService = exchangeAPIService;
    }

    @Override
    public Pair save(Pair pair) {
        log.debug("Request to save Pair : {}", pair);
        Optional<Pair> existPair = getByName(pair.getName());
        if(existPair.isPresent()) {
            pair.setId(existPair.get().getId());
        }
        return pairRepository.save(pair);
    }

    @Override
    public List<Pair> saveAll(List<Pair> pairs) {
        for(Pair pair : pairs){
            pair = save(pair);
        }
        return pairs;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Pair> findAll() {
        log.debug("Request to get all Pairs");
        return pairRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Pair> findOne(Long id) {
        log.debug("Request to get Pair : {}", id);
        return pairRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pair : {}", id);
        pairRepository.deleteById(id);
    }

    @Override
    public Optional<Pair> getByName(String name) {
        //TODO also use Optional for others GetBySymbol entites (proper)
        log.debug("Request to get Pair with name : {}", name);
        return pairRepository.getByName(name);
    }

    public List<Pair> createPairs(List<Symbol> symbols, List<Quote> quotes){
        List<Pair> pairs = new ArrayList<>();
        Pair pair;
        for(Symbol symbol: symbols){
            for(Quote quote : quotes){
                pair=new Pair(0F, symbol.getSymbol()+quote.getSymbol(), symbol, quote);
                log.debug("Creating Pair : {}", pair.getName());
                pairs.add(pair);
            }
        }
        saveAll(pairs);
        return pairs;
    }

    @Override
    public Pair updatePrice(Pair pair) {
        Pair updatedPricePair = exchangeAPIService.updatePairPrice(pair);
        if(updatedPricePair != null){
            updatedPricePair = save(updatedPricePair);
        }else if(pair.getQuote().getSymbol() == "USDT"){
            //cas ou la pair avec USDT n'existe pas sur l'exchange. On le simule en calculant a partir du prix de la pair BTC
            Pair pairBTC = getByName(pair.getSymbol().getSymbol()+"BTC").get();
            if(pairBTC != null) {
                Float priceBTC = exchangeAPIService.updateQuotePrice(new Quote("BTC", "Bitcoin", 0F)).getPriceUSDT();
                pair.setPrice(pairBTC.getPrice() * priceBTC);
                updatedPricePair = save(pair);
            }
        }else{
            delete(pair.getId());
        }
        return updatedPricePair;
    }

    @Override
    public List<Pair> updatePrices() {
        List<Pair> pairs = findAll();
        for(Pair pair : pairs){
            pair = updatePrice(pair);
        }
        return pairs;
    }

}
//TODO : update Asset and Wallet price/value
//TODO : faire une version plus legere ou l'on ne recupere que les pair en BTC, puis on actualise toutes les pairs USDT a partir du prix du BTC
//TODO : Deal with long time request to avoid API response like : (ex: updating all prices with Binance API)
//      "status": 504,
//    "error": "Gateway Timeout",
//    "message": "com.netflix.zuul.exception.ZuulException: Hystrix Readed time out",
