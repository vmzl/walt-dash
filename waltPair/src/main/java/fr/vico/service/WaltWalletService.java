package fr.vico.service;

import fr.vico.domain.Pair;
import fr.vico.domain.Symbol;
import org.springframework.cloud.openfeign.FeignClient;

import java.util.List;

public interface WaltWalletService {

    public List<Symbol> importSymbolsFromWaltWallet();

    public List<Pair> createOrUpdatePairs();
}
