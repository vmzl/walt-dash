package fr.vico.web.rest;

import fr.vico.WaltPairApp;
import fr.vico.domain.Symbol;
import fr.vico.repository.SymbolRepository;
import fr.vico.service.SymbolService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SymbolResource} REST controller.
 */
@SpringBootTest(classes = WaltPairApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class SymbolResourceIT {

    private static final String DEFAULT_SYMBOL = "AAAAAAAAAA";
    private static final String UPDATED_SYMBOL = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private SymbolRepository symbolRepository;

    @Autowired
    private SymbolService symbolService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSymbolMockMvc;

    private Symbol symbol;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Symbol createEntity(EntityManager em) {
        Symbol symbol = new Symbol()
            .symbol(DEFAULT_SYMBOL)
            .name(DEFAULT_NAME);
        return symbol;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Symbol createUpdatedEntity(EntityManager em) {
        Symbol symbol = new Symbol()
            .symbol(UPDATED_SYMBOL)
            .name(UPDATED_NAME);
        return symbol;
    }

    @BeforeEach
    public void initTest() {
        symbol = createEntity(em);
    }

    @Test
    @Transactional
    public void createSymbol() throws Exception {
        int databaseSizeBeforeCreate = symbolRepository.findAll().size();
        // Create the Symbol
        restSymbolMockMvc.perform(post("/api/symbols")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(symbol)))
            .andExpect(status().isCreated());

        // Validate the Symbol in the database
        List<Symbol> symbolList = symbolRepository.findAll();
        assertThat(symbolList).hasSize(databaseSizeBeforeCreate + 1);
        Symbol testSymbol = symbolList.get(symbolList.size() - 1);
        assertThat(testSymbol.getSymbol()).isEqualTo(DEFAULT_SYMBOL);
        assertThat(testSymbol.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createSymbolWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = symbolRepository.findAll().size();

        // Create the Symbol with an existing ID
        symbol.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSymbolMockMvc.perform(post("/api/symbols")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(symbol)))
            .andExpect(status().isBadRequest());

        // Validate the Symbol in the database
        List<Symbol> symbolList = symbolRepository.findAll();
        assertThat(symbolList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkSymbolIsRequired() throws Exception {
        int databaseSizeBeforeTest = symbolRepository.findAll().size();
        // set the field null
        symbol.setSymbol(null);

        // Create the Symbol, which fails.


        restSymbolMockMvc.perform(post("/api/symbols")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(symbol)))
            .andExpect(status().isBadRequest());

        List<Symbol> symbolList = symbolRepository.findAll();
        assertThat(symbolList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = symbolRepository.findAll().size();
        // set the field null
        symbol.setName(null);

        // Create the Symbol, which fails.


        restSymbolMockMvc.perform(post("/api/symbols")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(symbol)))
            .andExpect(status().isBadRequest());

        List<Symbol> symbolList = symbolRepository.findAll();
        assertThat(symbolList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSymbols() throws Exception {
        // Initialize the database
        symbolRepository.saveAndFlush(symbol);

        // Get all the symbolList
        restSymbolMockMvc.perform(get("/api/symbols?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(symbol.getId().intValue())))
            .andExpect(jsonPath("$.[*].symbol").value(hasItem(DEFAULT_SYMBOL)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getSymbol() throws Exception {
        // Initialize the database
        symbolRepository.saveAndFlush(symbol);

        // Get the symbol
        restSymbolMockMvc.perform(get("/api/symbols/{id}", symbol.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(symbol.getId().intValue()))
            .andExpect(jsonPath("$.symbol").value(DEFAULT_SYMBOL))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingSymbol() throws Exception {
        // Get the symbol
        restSymbolMockMvc.perform(get("/api/symbols/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSymbol() throws Exception {
        // Initialize the database
        symbolService.save(symbol);

        int databaseSizeBeforeUpdate = symbolRepository.findAll().size();

        // Update the symbol
        Symbol updatedSymbol = symbolRepository.findById(symbol.getId()).get();
        // Disconnect from session so that the updates on updatedSymbol are not directly saved in db
        em.detach(updatedSymbol);
        updatedSymbol
            .symbol(UPDATED_SYMBOL)
            .name(UPDATED_NAME);

        restSymbolMockMvc.perform(put("/api/symbols")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSymbol)))
            .andExpect(status().isOk());

        // Validate the Symbol in the database
        List<Symbol> symbolList = symbolRepository.findAll();
        assertThat(symbolList).hasSize(databaseSizeBeforeUpdate);
        Symbol testSymbol = symbolList.get(symbolList.size() - 1);
        assertThat(testSymbol.getSymbol()).isEqualTo(UPDATED_SYMBOL);
        assertThat(testSymbol.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingSymbol() throws Exception {
        int databaseSizeBeforeUpdate = symbolRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSymbolMockMvc.perform(put("/api/symbols")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(symbol)))
            .andExpect(status().isBadRequest());

        // Validate the Symbol in the database
        List<Symbol> symbolList = symbolRepository.findAll();
        assertThat(symbolList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSymbol() throws Exception {
        // Initialize the database
        symbolService.save(symbol);

        int databaseSizeBeforeDelete = symbolRepository.findAll().size();

        // Delete the symbol
        restSymbolMockMvc.perform(delete("/api/symbols/{id}", symbol.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Symbol> symbolList = symbolRepository.findAll();
        assertThat(symbolList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
