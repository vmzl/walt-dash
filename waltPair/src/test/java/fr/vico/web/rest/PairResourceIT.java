package fr.vico.web.rest;

import fr.vico.WaltPairApp;
import fr.vico.domain.Pair;
import fr.vico.repository.PairRepository;
import fr.vico.service.PairService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PairResource} REST controller.
 */
@SpringBootTest(classes = WaltPairApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PairResourceIT {

    private static final Float DEFAULT_PRICE = 1F;
    private static final Float UPDATED_PRICE = 2F;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private PairRepository pairRepository;

    @Autowired
    private PairService pairService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPairMockMvc;

    private Pair pair;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pair createEntity(EntityManager em) {
        Pair pair = new Pair()
            .price(DEFAULT_PRICE)
            .name(DEFAULT_NAME);
        return pair;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pair createUpdatedEntity(EntityManager em) {
        Pair pair = new Pair()
            .price(UPDATED_PRICE)
            .name(UPDATED_NAME);
        return pair;
    }

    @BeforeEach
    public void initTest() {
        pair = createEntity(em);
    }

    @Test
    @Transactional
    public void createPair() throws Exception {
        int databaseSizeBeforeCreate = pairRepository.findAll().size();
        // Create the Pair
        restPairMockMvc.perform(post("/api/pairs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pair)))
            .andExpect(status().isCreated());

        // Validate the Pair in the database
        List<Pair> pairList = pairRepository.findAll();
        assertThat(pairList).hasSize(databaseSizeBeforeCreate + 1);
        Pair testPair = pairList.get(pairList.size() - 1);
        assertThat(testPair.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testPair.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createPairWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pairRepository.findAll().size();

        // Create the Pair with an existing ID
        pair.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPairMockMvc.perform(post("/api/pairs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pair)))
            .andExpect(status().isBadRequest());

        // Validate the Pair in the database
        List<Pair> pairList = pairRepository.findAll();
        assertThat(pairList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPairs() throws Exception {
        // Initialize the database
        pairRepository.saveAndFlush(pair);

        // Get all the pairList
        restPairMockMvc.perform(get("/api/pairs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pair.getId().intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getPair() throws Exception {
        // Initialize the database
        pairRepository.saveAndFlush(pair);

        // Get the pair
        restPairMockMvc.perform(get("/api/pairs/{id}", pair.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pair.getId().intValue()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingPair() throws Exception {
        // Get the pair
        restPairMockMvc.perform(get("/api/pairs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePair() throws Exception {
        // Initialize the database
        pairService.save(pair);

        int databaseSizeBeforeUpdate = pairRepository.findAll().size();

        // Update the pair
        Pair updatedPair = pairRepository.findById(pair.getId()).get();
        // Disconnect from session so that the updates on updatedPair are not directly saved in db
        em.detach(updatedPair);
        updatedPair
            .price(UPDATED_PRICE)
            .name(UPDATED_NAME);

        restPairMockMvc.perform(put("/api/pairs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPair)))
            .andExpect(status().isOk());

        // Validate the Pair in the database
        List<Pair> pairList = pairRepository.findAll();
        assertThat(pairList).hasSize(databaseSizeBeforeUpdate);
        Pair testPair = pairList.get(pairList.size() - 1);
        assertThat(testPair.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testPair.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingPair() throws Exception {
        int databaseSizeBeforeUpdate = pairRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPairMockMvc.perform(put("/api/pairs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pair)))
            .andExpect(status().isBadRequest());

        // Validate the Pair in the database
        List<Pair> pairList = pairRepository.findAll();
        assertThat(pairList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePair() throws Exception {
        // Initialize the database
        pairService.save(pair);

        int databaseSizeBeforeDelete = pairRepository.findAll().size();

        // Delete the pair
        restPairMockMvc.perform(delete("/api/pairs/{id}", pair.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pair> pairList = pairRepository.findAll();
        assertThat(pairList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
