package fr.vico.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.vico.web.rest.TestUtil;

public class PairTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pair.class);
        Pair pair1 = new Pair();
        pair1.setId(1L);
        Pair pair2 = new Pair();
        pair2.setId(pair1.getId());
        assertThat(pair1).isEqualTo(pair2);
        pair2.setId(2L);
        assertThat(pair1).isNotEqualTo(pair2);
        pair1.setId(null);
        assertThat(pair1).isNotEqualTo(pair2);
    }
}
