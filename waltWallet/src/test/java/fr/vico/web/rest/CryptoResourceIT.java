package fr.vico.web.rest;

import fr.vico.WaltWalletApp;
import fr.vico.domain.Crypto;
import fr.vico.repository.CryptoRepository;
import fr.vico.service.CryptoService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CryptoResource} REST controller.
 */
@SpringBootTest(classes = WaltWalletApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CryptoResourceIT {

    private static final String DEFAULT_SYMBOL = "AAAAAAAAAA";
    private static final String UPDATED_SYMBOL = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private CryptoRepository cryptoRepository;

    @Autowired
    private CryptoService cryptoService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCryptoMockMvc;

    private Crypto crypto;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Crypto createEntity(EntityManager em) {
        Crypto crypto = new Crypto()
            .symbol(DEFAULT_SYMBOL)
            .name(DEFAULT_NAME);
        return crypto;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Crypto createUpdatedEntity(EntityManager em) {
        Crypto crypto = new Crypto()
            .symbol(UPDATED_SYMBOL)
            .name(UPDATED_NAME);
        return crypto;
    }

    @BeforeEach
    public void initTest() {
        crypto = createEntity(em);
    }

    @Test
    @Transactional
    public void createCrypto() throws Exception {
        int databaseSizeBeforeCreate = cryptoRepository.findAll().size();
        // Create the Crypto
        restCryptoMockMvc.perform(post("/api/cryptos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(crypto)))
            .andExpect(status().isCreated());

        // Validate the Crypto in the database
        List<Crypto> cryptoList = cryptoRepository.findAll();
        assertThat(cryptoList).hasSize(databaseSizeBeforeCreate + 1);
        Crypto testCrypto = cryptoList.get(cryptoList.size() - 1);
        assertThat(testCrypto.getSymbol()).isEqualTo(DEFAULT_SYMBOL);
        assertThat(testCrypto.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createCryptoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cryptoRepository.findAll().size();

        // Create the Crypto with an existing ID
        crypto.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCryptoMockMvc.perform(post("/api/cryptos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(crypto)))
            .andExpect(status().isBadRequest());

        // Validate the Crypto in the database
        List<Crypto> cryptoList = cryptoRepository.findAll();
        assertThat(cryptoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkSymbolIsRequired() throws Exception {
        int databaseSizeBeforeTest = cryptoRepository.findAll().size();
        // set the field null
        crypto.setSymbol(null);

        // Create the Crypto, which fails.


        restCryptoMockMvc.perform(post("/api/cryptos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(crypto)))
            .andExpect(status().isBadRequest());

        List<Crypto> cryptoList = cryptoRepository.findAll();
        assertThat(cryptoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = cryptoRepository.findAll().size();
        // set the field null
        crypto.setName(null);

        // Create the Crypto, which fails.


        restCryptoMockMvc.perform(post("/api/cryptos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(crypto)))
            .andExpect(status().isBadRequest());

        List<Crypto> cryptoList = cryptoRepository.findAll();
        assertThat(cryptoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCryptos() throws Exception {
        // Initialize the database
        cryptoRepository.saveAndFlush(crypto);

        // Get all the cryptoList
        restCryptoMockMvc.perform(get("/api/cryptos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(crypto.getId().intValue())))
            .andExpect(jsonPath("$.[*].symbol").value(hasItem(DEFAULT_SYMBOL)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getCrypto() throws Exception {
        // Initialize the database
        cryptoRepository.saveAndFlush(crypto);

        // Get the crypto
        restCryptoMockMvc.perform(get("/api/cryptos/{id}", crypto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(crypto.getId().intValue()))
            .andExpect(jsonPath("$.symbol").value(DEFAULT_SYMBOL))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingCrypto() throws Exception {
        // Get the crypto
        restCryptoMockMvc.perform(get("/api/cryptos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCrypto() throws Exception {
        // Initialize the database
        cryptoService.save(crypto);

        int databaseSizeBeforeUpdate = cryptoRepository.findAll().size();

        // Update the crypto
        Crypto updatedCrypto = cryptoRepository.findById(crypto.getId()).get();
        // Disconnect from session so that the updates on updatedCrypto are not directly saved in db
        em.detach(updatedCrypto);
        updatedCrypto
            .symbol(UPDATED_SYMBOL)
            .name(UPDATED_NAME);

        restCryptoMockMvc.perform(put("/api/cryptos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCrypto)))
            .andExpect(status().isOk());

        // Validate the Crypto in the database
        List<Crypto> cryptoList = cryptoRepository.findAll();
        assertThat(cryptoList).hasSize(databaseSizeBeforeUpdate);
        Crypto testCrypto = cryptoList.get(cryptoList.size() - 1);
        assertThat(testCrypto.getSymbol()).isEqualTo(UPDATED_SYMBOL);
        assertThat(testCrypto.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingCrypto() throws Exception {
        int databaseSizeBeforeUpdate = cryptoRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCryptoMockMvc.perform(put("/api/cryptos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(crypto)))
            .andExpect(status().isBadRequest());

        // Validate the Crypto in the database
        List<Crypto> cryptoList = cryptoRepository.findAll();
        assertThat(cryptoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCrypto() throws Exception {
        // Initialize the database
        cryptoService.save(crypto);

        int databaseSizeBeforeDelete = cryptoRepository.findAll().size();

        // Delete the crypto
        restCryptoMockMvc.perform(delete("/api/cryptos/{id}", crypto.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Crypto> cryptoList = cryptoRepository.findAll();
        assertThat(cryptoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
