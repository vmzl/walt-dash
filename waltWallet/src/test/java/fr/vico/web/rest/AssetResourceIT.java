package fr.vico.web.rest;

import fr.vico.WaltWalletApp;
import fr.vico.domain.Asset;
import fr.vico.domain.Crypto;
import fr.vico.repository.AssetRepository;
import fr.vico.service.AssetService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AssetResource} REST controller.
 */
@SpringBootTest(classes = WaltWalletApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AssetResourceIT {

    private static final Float DEFAULT_TOTAL = 1F;
    private static final Float UPDATED_TOTAL = 2F;

    private static final Float DEFAULT_LOCKED = 1F;
    private static final Float UPDATED_LOCKED = 2F;

    private static final Float DEFAULT_PRICE_BTC = 1F;
    private static final Float UPDATED_PRICE_BTC = 2F;

    private static final Float DEFAULT_PRICE_USDT = 1F;
    private static final Float UPDATED_PRICE_USDT = 2F;

    private static final Float DEFAULT_PRICE_BTC_24_H = 1F;
    private static final Float UPDATED_PRICE_BTC_24_H = 2F;

    private static final Float DEFAULT_PRICE_USDT_24_H = 1F;
    private static final Float UPDATED_PRICE_USDT_24_H = 2F;

    private static final LocalDate DEFAULT_LAST_UPDATED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private AssetRepository assetRepository;

    @Autowired
    private AssetService assetService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAssetMockMvc;

    private Asset asset;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Asset createEntity(EntityManager em) {
        Asset asset = new Asset()
            .total(DEFAULT_TOTAL)
            .locked(DEFAULT_LOCKED)
            .priceBTC(DEFAULT_PRICE_BTC)
            .priceUSDT(DEFAULT_PRICE_USDT)
            .priceBTC24H(DEFAULT_PRICE_BTC_24_H)
            .priceUSDT24H(DEFAULT_PRICE_USDT_24_H)
            .lastUpdated(DEFAULT_LAST_UPDATED);
        // Add required entity
        Crypto crypto;
        if (TestUtil.findAll(em, Crypto.class).isEmpty()) {
            crypto = CryptoResourceIT.createEntity(em);
            em.persist(crypto);
            em.flush();
        } else {
            crypto = TestUtil.findAll(em, Crypto.class).get(0);
        }
        asset.setCrypto(crypto);
        return asset;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Asset createUpdatedEntity(EntityManager em) {
        Asset asset = new Asset()
            .total(UPDATED_TOTAL)
            .locked(UPDATED_LOCKED)
            .priceBTC(UPDATED_PRICE_BTC)
            .priceUSDT(UPDATED_PRICE_USDT)
            .priceBTC24H(UPDATED_PRICE_BTC_24_H)
            .priceUSDT24H(UPDATED_PRICE_USDT_24_H)
            .lastUpdated(UPDATED_LAST_UPDATED);
        // Add required entity
        Crypto crypto;
        if (TestUtil.findAll(em, Crypto.class).isEmpty()) {
            crypto = CryptoResourceIT.createUpdatedEntity(em);
            em.persist(crypto);
            em.flush();
        } else {
            crypto = TestUtil.findAll(em, Crypto.class).get(0);
        }
        asset.setCrypto(crypto);
        return asset;
    }

    @BeforeEach
    public void initTest() {
        asset = createEntity(em);
    }

    @Test
    @Transactional
    public void createAsset() throws Exception {
        int databaseSizeBeforeCreate = assetRepository.findAll().size();
        // Create the Asset
        restAssetMockMvc.perform(post("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(asset)))
            .andExpect(status().isCreated());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeCreate + 1);
        Asset testAsset = assetList.get(assetList.size() - 1);
        assertThat(testAsset.getTotal()).isEqualTo(DEFAULT_TOTAL);
        assertThat(testAsset.getLocked()).isEqualTo(DEFAULT_LOCKED);
        assertThat(testAsset.getPriceBTC()).isEqualTo(DEFAULT_PRICE_BTC);
        assertThat(testAsset.getPriceUSDT()).isEqualTo(DEFAULT_PRICE_USDT);
        assertThat(testAsset.getPriceBTC24H()).isEqualTo(DEFAULT_PRICE_BTC_24_H);
        assertThat(testAsset.getPriceUSDT24H()).isEqualTo(DEFAULT_PRICE_USDT_24_H);
        assertThat(testAsset.getLastUpdated()).isEqualTo(DEFAULT_LAST_UPDATED);
    }

    @Test
    @Transactional
    public void createAssetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = assetRepository.findAll().size();

        // Create the Asset with an existing ID
        asset.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssetMockMvc.perform(post("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(asset)))
            .andExpect(status().isBadRequest());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTotalIsRequired() throws Exception {
        int databaseSizeBeforeTest = assetRepository.findAll().size();
        // set the field null
        asset.setTotal(null);

        // Create the Asset, which fails.


        restAssetMockMvc.perform(post("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(asset)))
            .andExpect(status().isBadRequest());

        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAssets() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        // Get all the assetList
        restAssetMockMvc.perform(get("/api/assets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(asset.getId().intValue())))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.doubleValue())))
            .andExpect(jsonPath("$.[*].locked").value(hasItem(DEFAULT_LOCKED.doubleValue())))
            .andExpect(jsonPath("$.[*].priceBTC").value(hasItem(DEFAULT_PRICE_BTC.doubleValue())))
            .andExpect(jsonPath("$.[*].priceUSDT").value(hasItem(DEFAULT_PRICE_USDT.doubleValue())))
            .andExpect(jsonPath("$.[*].priceBTC24H").value(hasItem(DEFAULT_PRICE_BTC_24_H.doubleValue())))
            .andExpect(jsonPath("$.[*].priceUSDT24H").value(hasItem(DEFAULT_PRICE_USDT_24_H.doubleValue())))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(DEFAULT_LAST_UPDATED.toString())));
    }
    
    @Test
    @Transactional
    public void getAsset() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        // Get the asset
        restAssetMockMvc.perform(get("/api/assets/{id}", asset.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(asset.getId().intValue()))
            .andExpect(jsonPath("$.total").value(DEFAULT_TOTAL.doubleValue()))
            .andExpect(jsonPath("$.locked").value(DEFAULT_LOCKED.doubleValue()))
            .andExpect(jsonPath("$.priceBTC").value(DEFAULT_PRICE_BTC.doubleValue()))
            .andExpect(jsonPath("$.priceUSDT").value(DEFAULT_PRICE_USDT.doubleValue()))
            .andExpect(jsonPath("$.priceBTC24H").value(DEFAULT_PRICE_BTC_24_H.doubleValue()))
            .andExpect(jsonPath("$.priceUSDT24H").value(DEFAULT_PRICE_USDT_24_H.doubleValue()))
            .andExpect(jsonPath("$.lastUpdated").value(DEFAULT_LAST_UPDATED.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingAsset() throws Exception {
        // Get the asset
        restAssetMockMvc.perform(get("/api/assets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAsset() throws Exception {
        // Initialize the database
        assetService.save(asset);

        int databaseSizeBeforeUpdate = assetRepository.findAll().size();

        // Update the asset
        Asset updatedAsset = assetRepository.findById(asset.getId()).get();
        // Disconnect from session so that the updates on updatedAsset are not directly saved in db
        em.detach(updatedAsset);
        updatedAsset
            .total(UPDATED_TOTAL)
            .locked(UPDATED_LOCKED)
            .priceBTC(UPDATED_PRICE_BTC)
            .priceUSDT(UPDATED_PRICE_USDT)
            .priceBTC24H(UPDATED_PRICE_BTC_24_H)
            .priceUSDT24H(UPDATED_PRICE_USDT_24_H)
            .lastUpdated(UPDATED_LAST_UPDATED);

        restAssetMockMvc.perform(put("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAsset)))
            .andExpect(status().isOk());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
        Asset testAsset = assetList.get(assetList.size() - 1);
        assertThat(testAsset.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testAsset.getLocked()).isEqualTo(UPDATED_LOCKED);
        assertThat(testAsset.getPriceBTC()).isEqualTo(UPDATED_PRICE_BTC);
        assertThat(testAsset.getPriceUSDT()).isEqualTo(UPDATED_PRICE_USDT);
        assertThat(testAsset.getPriceBTC24H()).isEqualTo(UPDATED_PRICE_BTC_24_H);
        assertThat(testAsset.getPriceUSDT24H()).isEqualTo(UPDATED_PRICE_USDT_24_H);
        assertThat(testAsset.getLastUpdated()).isEqualTo(UPDATED_LAST_UPDATED);
    }

    @Test
    @Transactional
    public void updateNonExistingAsset() throws Exception {
        int databaseSizeBeforeUpdate = assetRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssetMockMvc.perform(put("/api/assets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(asset)))
            .andExpect(status().isBadRequest());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAsset() throws Exception {
        // Initialize the database
        assetService.save(asset);

        int databaseSizeBeforeDelete = assetRepository.findAll().size();

        // Delete the asset
        restAssetMockMvc.perform(delete("/api/assets/{id}", asset.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
