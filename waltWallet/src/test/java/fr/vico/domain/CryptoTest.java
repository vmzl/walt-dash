package fr.vico.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.vico.web.rest.TestUtil;

public class CryptoTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Crypto.class);
        Crypto crypto1 = new Crypto();
        crypto1.setId(1L);
        Crypto crypto2 = new Crypto();
        crypto2.setId(crypto1.getId());
        assertThat(crypto1).isEqualTo(crypto2);
        crypto2.setId(2L);
        assertThat(crypto1).isNotEqualTo(crypto2);
        crypto1.setId(null);
        assertThat(crypto1).isNotEqualTo(crypto2);
    }
}
