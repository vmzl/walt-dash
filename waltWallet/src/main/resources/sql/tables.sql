DROP TABLE IF EXISTS `asset`;
DROP TABLE IF EXISTS `crypto`;
DROP TABLE IF EXISTS `wallet`;

CREATE TABLE `crypto` (
    `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `symbol` varchar(20) NOT NULL,
    `name` varchar(20) NOT NULL
) ;

CREATE TABLE `wallet` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(20),
  `value` float DEFAULT NULL,
  `last_updated` date
) ;

CREATE TABLE `asset` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `wallet_id` int DEFAULT NULL,
  `crypto_id` int NOT NULL,
  `total` float NOT NULL,
  `locked` float,
  `price_btc` float,
  `price_usdt` float,
  `price_btc_24_h` float,
  `price_usdt_24_h` float,
  `last_updated` date,
  FOREIGN KEY (wallet_id) REFERENCES wallet(id),
  FOREIGN KEY (crypto_id) REFERENCES crypto(id)
) ;

INSERT INTO `crypto` VALUES
    (1, 'BTC', 'Bitcoin'),
    (2, 'ETH', 'Etherium'),
    (3, 'XMR', 'Monero');

INSERT INTO `wallet` VALUES
    (1, 'first', 0, NOW());

INSERT INTO `asset` VALUES
    (1, 1, 1, 0.02, 0, 1, 14000, 0, 0, NOW()),
    (2, 1, 2, 0.8, 0, 0.2, 300, 0, 0, NOW());
