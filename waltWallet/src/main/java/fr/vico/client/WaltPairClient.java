package fr.vico.client;

import feign.Param;
import feign.RequestLine;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

public interface WaltPairClient {

    @RequestLine("GET /pairs/symbol/{symbol}")
    String getPairsWithSymbol(@Param("symbol") String symbol);
}
