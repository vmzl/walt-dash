package fr.vico.service;

import fr.vico.domain.Wallet;

public interface ExchangeAPIService {

    public Wallet createWalletAccount();

    public void updatePricesWallet(Wallet wallet);
}
