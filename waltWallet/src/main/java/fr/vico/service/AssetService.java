package fr.vico.service;

import fr.vico.domain.Asset;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Asset}.
 */
public interface AssetService {

    /**
     * Save a asset.
     *
     * @param asset the entity to save.
     * @return the persisted entity.
     */
    Asset save(Asset asset);

    public List<Asset> saveAll(List<Asset> assets);

    /**
     * Get all the assets.
     *
     * @return the list of entities.
     */
    List<Asset> findAll();


    /**
     * Get the "id" asset.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Asset> findOne(Long id);

    /**
     * Delete the "id" asset.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get the asset with the crypto "symbol".
     *
     * @param symbol of the crypto of the asset.
     * @return the entity.
     */
    List<Asset> getAssetsBySymbol(String symbol);
}
