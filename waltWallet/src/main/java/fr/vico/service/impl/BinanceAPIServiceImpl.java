package fr.vico.service.impl;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.account.Account;
import com.binance.api.client.domain.account.AssetBalance;
import fr.vico.domain.Asset;
import fr.vico.domain.Crypto;
import fr.vico.domain.Wallet;
import fr.vico.service.AssetService;
import fr.vico.service.CryptoService;
import fr.vico.service.ExchangeAPIService;
import fr.vico.service.WalletService;
import fr.vico.web.rest.WalletResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BinanceAPIServiceImpl implements ExchangeAPIService {

    private final Logger log = LoggerFactory.getLogger(BinanceAPIServiceImpl.class);

    private BinanceApiRestClient client;
    private final WalletService walletService;
    private final AssetService assetService;
    private final CryptoService cryptoService;

    public BinanceAPIServiceImpl(WalletService walletService, AssetService assetService, CryptoService cryptoService) {
        BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance("Q0HfKdFIZlHLYF2hddCufk7lqlZEg8Sz1zeWjH3bYHftNgcaTcZrpgeatewNUUF5", "6UAGjA3fypSkUqSjg17YlYpTE0ac6YW38saUSYSVYXGfz7Z1INF49hHyGi18pcLV");
        this.client = factory.newRestClient();
        this.walletService = walletService;
        this.assetService = assetService;
        this.cryptoService = cryptoService;
    }

    @Override
    public Wallet createWalletAccount() {
        Account account = client.getAccount();
        List<AssetBalance> balances = account.getBalances();
        Wallet wallet = new Wallet();
        wallet.setName("Binance");
        wallet = walletService.save(wallet);
        for(AssetBalance balance : balances){
            if(Float.parseFloat(balance.getFree())+Float.parseFloat(balance.getLocked())>0){
                Asset tAsset = new Asset();
                Crypto tCrypto = new Crypto();

                tAsset.setWallet(wallet);
                tAsset.setTotal(Float.parseFloat(balance.getFree())+Float.parseFloat(balance.getLocked()));
                tAsset.setLocked(Float.parseFloat(balance.getLocked()));

                tCrypto.setSymbol(balance.getAsset());
                tCrypto.setName("UNKNOW");//TODO recuperer le nom avec l'API coinmarkcap


                tCrypto = cryptoService.save(tCrypto);
                tAsset.setCrypto(tCrypto);

                tAsset = assetService.save(tAsset);
                wallet.addAsset(tAsset);
            }
        }
        updatePricesWallet(wallet);
        log.debug(wallet.toString());
        walletService.save(wallet);
        return wallet;

    }

    @Override
    public void updatePricesWallet(Wallet wallet) {

    }
}
