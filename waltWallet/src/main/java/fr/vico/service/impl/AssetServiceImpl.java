package fr.vico.service.impl;

import fr.vico.service.AssetService;
import fr.vico.domain.Asset;
import fr.vico.repository.AssetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Asset}.
 */
@Service
@Transactional
public class AssetServiceImpl implements AssetService {

    private final Logger log = LoggerFactory.getLogger(AssetServiceImpl.class);

    private final AssetRepository assetRepository;

    public AssetServiceImpl(AssetRepository assetRepository) {
        this.assetRepository = assetRepository;
    }

    @Override
    public Asset save(Asset asset) {
        log.debug("Request to save Asset : {}", asset);
        return assetRepository.save(asset);
    }

    @Override
    public List<Asset> saveAll(List<Asset> assets) {
        for(Asset asset : assets){
            asset = save(asset);
        }
        return assets;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Asset> findAll() {
        log.debug("Request to get all Assets");
        return assetRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Asset> findOne(Long id) {
        log.debug("Request to get Asset : {}", id);
        return assetRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Asset : {}", id);
        assetRepository.deleteById(id);
    }

    @Override
    public List<Asset> getAssetsBySymbol(String symbol) {
        log.debug("Request to get Asset with Crypto symbol : {}", symbol);
        return assetRepository.getAssetsBySymbol(symbol);
    }
}
