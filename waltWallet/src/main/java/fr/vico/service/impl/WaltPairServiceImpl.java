package fr.vico.service.impl;

import com.google.gson.*;
import feign.Feign;
import feign.slf4j.Slf4jLogger;
import fr.vico.client.UserFeignClientInterceptor;
import fr.vico.client.WaltPairClient;
import fr.vico.domain.Asset;
import fr.vico.service.AssetService;
import fr.vico.service.WaltPairService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WaltPairServiceImpl implements WaltPairService {

    private static final String WALT_PAIR_URL = "http://localhost:8310/services/waltpair/api";

    private final Logger log = LoggerFactory.getLogger(WaltPairServiceImpl.class);

    private final AssetService assetService;

    public WaltPairServiceImpl(AssetService assetService) {
        this.assetService = assetService;
    }


    @Override
    public List<Asset> updatePrice(String symbol) {

        WaltPairClient waltPairClient = Feign.builder()
            //.client(new OkHttpClient()) //I let both because they can be usefull for others feignclient fonctions ...?
            //.encoder(new GsonEncoder())
            //.decoder(new GsonDecoder())
            .logger(new Slf4jLogger(WaltPairClient.class))
            .logLevel(feign.Logger.Level.FULL)
            .requestInterceptor(new UserFeignClientInterceptor())
            .target(WaltPairClient.class, WALT_PAIR_URL);

        String json = waltPairClient.getPairsWithSymbol(symbol);

        List<Map<Object, Object>> javaRootMapObject = new Gson().fromJson(json, List.class);

        String price = "", quote = "", priceBTC = "", priceUSDT = "";

        for(Map<Object, Object> map : javaRootMapObject){
            log.debug(map.get("name").toString());
            log.debug(String.valueOf(map.get("price")));
            log.debug(((Map)map.get("quote")).get("symbol").toString());
            price=String.valueOf(map.get("price"));
            quote=((Map)map.get("quote")).get("symbol").toString();
            switch (quote){
                case "BTC":
                    priceBTC = price;
                    break;
                case "USDT":
                    priceUSDT = price;
                    break;
                default:
                    log.debug("Unknown quote : "+quote);
                    break;
            }


        }

        List<Asset> assets = assetService.getAssetsBySymbol(symbol);
        for(Asset asset : assets){
            asset.setPriceBTC(Float.parseFloat(priceBTC));
            asset.setPriceUSDT(Float.parseFloat(priceUSDT));
        }
        return assetService.saveAll(assets);
    }
}
