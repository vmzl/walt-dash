package fr.vico.service.impl;

import fr.vico.service.CryptoService;
import fr.vico.domain.Crypto;
import fr.vico.repository.CryptoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Crypto}.
 */
@Service
@Transactional
public class CryptoServiceImpl implements CryptoService {

    private final Logger log = LoggerFactory.getLogger(CryptoServiceImpl.class);

    private final CryptoRepository cryptoRepository;

    public CryptoServiceImpl(CryptoRepository cryptoRepository) {
        this.cryptoRepository = cryptoRepository;
    }

    @Override
    public Crypto save(Crypto crypto) {
        log.debug("Request to save Crypto : {}", crypto);
        Crypto existCrypto = getBySymbol(crypto.getSymbol());
        if(existCrypto!=null) {
            crypto = existCrypto;
        }
        return cryptoRepository.save(crypto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Crypto> findAll() {
        log.debug("Request to get all Cryptos");
        return cryptoRepository.findAll();
    }

    @Override
    public Crypto getBySymbol(String symbol) {
        log.debug("Request to get Crypto with symbol : {}", symbol);
        return cryptoRepository.getBySymbol(symbol);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Crypto> findOne(Long id) {
        log.debug("Request to get Crypto : {}", id);
        return cryptoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Crypto : {}", id);
        cryptoRepository.deleteById(id);
    }
}
