package fr.vico.service;

import fr.vico.domain.Crypto;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Crypto}.
 */
public interface CryptoService {

    /**
     * Save a crypto.
     *
     * @param crypto the entity to save.
     * @return the persisted entity.
     */
    Crypto save(Crypto crypto);

    /**
     * Get all the cryptos.
     *
     * @return the list of entities.
     */
    List<Crypto> findAll();

    Crypto getBySymbol(String symbol);


    /**
     * Get the "id" crypto.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Crypto> findOne(Long id);

    /**
     * Delete the "id" crypto.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
