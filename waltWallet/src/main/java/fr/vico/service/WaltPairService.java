package fr.vico.service;

import fr.vico.domain.Asset;

import java.util.List;

public interface WaltPairService {
    List<Asset> updatePrice(String symbol);
}
