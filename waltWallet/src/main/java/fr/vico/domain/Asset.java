package fr.vico.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Asset.
 */
@Entity
@Table(name = "asset")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Asset implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "total", nullable = false)
    private Float total;

    @Column(name = "locked")
    private Float locked;

    @Column(name = "price_btc")
    private Float priceBTC;

    @Column(name = "price_usdt")
    private Float priceUSDT;

    @Column(name = "price_btc_24_h")
    private Float priceBTC24H;

    @Column(name = "price_usdt_24_h")
    private Float priceUSDT24H;

    @Column(name = "last_updated")
    private LocalDate lastUpdated;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "assets", allowSetters = true)
    private Crypto crypto;

    @ManyToOne
    @JsonIgnoreProperties(value = "assets", allowSetters = true)
    private Wallet wallet;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getTotal() {
        return total;
    }

    public Asset total(Float total) {
        this.total = total;
        return this;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public Float getLocked() {
        return locked;
    }

    public Asset locked(Float locked) {
        this.locked = locked;
        return this;
    }

    public void setLocked(Float locked) {
        this.locked = locked;
    }

    public Float getPriceBTC() {
        return priceBTC;
    }

    public Asset priceBTC(Float priceBTC) {
        this.priceBTC = priceBTC;
        return this;
    }

    public void setPriceBTC(Float priceBTC) {
        this.priceBTC = priceBTC;
    }

    public Float getPriceUSDT() {
        return priceUSDT;
    }

    public Asset priceUSDT(Float priceUSDT) {
        this.priceUSDT = priceUSDT;
        return this;
    }

    public void setPriceUSDT(Float priceUSDT) {
        this.priceUSDT = priceUSDT;
    }

    public Float getPriceBTC24H() {
        return priceBTC24H;
    }

    public Asset priceBTC24H(Float priceBTC24H) {
        this.priceBTC24H = priceBTC24H;
        return this;
    }

    public void setPriceBTC24H(Float priceBTC24H) {
        this.priceBTC24H = priceBTC24H;
    }

    public Float getPriceUSDT24H() {
        return priceUSDT24H;
    }

    public Asset priceUSDT24H(Float priceUSDT24H) {
        this.priceUSDT24H = priceUSDT24H;
        return this;
    }

    public void setPriceUSDT24H(Float priceUSDT24H) {
        this.priceUSDT24H = priceUSDT24H;
    }

    public LocalDate getLastUpdated() {
        return lastUpdated;
    }

    public Asset lastUpdated(LocalDate lastUpdated) {
        this.lastUpdated = lastUpdated;
        return this;
    }

    public void setLastUpdated(LocalDate lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Crypto getCrypto() {
        return crypto;
    }

    public Asset crypto(Crypto crypto) {
        this.crypto = crypto;
        return this;
    }

    public void setCrypto(Crypto crypto) {
        this.crypto = crypto;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public Asset wallet(Wallet wallet) {
        this.wallet = wallet;
        return this;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Asset)) {
            return false;
        }
        return id != null && id.equals(((Asset) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Asset{" +
            "id=" + getId() +
            ", total=" + getTotal() +
            ", locked=" + getLocked() +
            ", priceBTC=" + getPriceBTC() +
            ", priceUSDT=" + getPriceUSDT() +
            ", priceBTC24H=" + getPriceBTC24H() +
            ", priceUSDT24H=" + getPriceUSDT24H() +
            ", lastUpdated='" + getLastUpdated() + "'" +
            "}";
    }
}
