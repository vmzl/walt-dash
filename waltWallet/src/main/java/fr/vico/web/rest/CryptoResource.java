package fr.vico.web.rest;

import fr.vico.domain.Crypto;
import fr.vico.service.CryptoService;
import fr.vico.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link fr.vico.domain.Crypto}.
 */
@RestController
@RequestMapping("/api")
public class CryptoResource {

    private final Logger log = LoggerFactory.getLogger(CryptoResource.class);

    private static final String ENTITY_NAME = "waltWalletCrypto";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CryptoService cryptoService;

    public CryptoResource(CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }

    /**
     * {@code POST  /cryptos} : Create a new crypto.
     *
     * @param crypto the crypto to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new crypto, or with status {@code 400 (Bad Request)} if the crypto has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cryptos")
    public ResponseEntity<Crypto> createCrypto(@Valid @RequestBody Crypto crypto) throws URISyntaxException {
        log.debug("REST request to save Crypto : {}", crypto);
        if (crypto.getId() != null) {
            throw new BadRequestAlertException("A new crypto cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Crypto result = cryptoService.save(crypto);
        return ResponseEntity.created(new URI("/api/cryptos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cryptos} : Updates an existing crypto.
     *
     * @param crypto the crypto to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated crypto,
     * or with status {@code 400 (Bad Request)} if the crypto is not valid,
     * or with status {@code 500 (Internal Server Error)} if the crypto couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cryptos")
    public ResponseEntity<Crypto> updateCrypto(@Valid @RequestBody Crypto crypto) throws URISyntaxException {
        log.debug("REST request to update Crypto : {}", crypto);
        if (crypto.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Crypto result = cryptoService.save(crypto);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, crypto.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cryptos} : get all the cryptos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cryptos in body.
     */
    @GetMapping("/cryptos")
    public List<Crypto> getAllCryptos() {
        log.debug("REST request to get all Cryptos");
        return cryptoService.findAll();
    }

    /**
     * {@code GET  /cryptos/:id} : get the "id" crypto.
     *
     * @param id the id of the crypto to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the crypto, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cryptos/{id}")
    public ResponseEntity<Crypto> getCrypto(@PathVariable Long id) {
        log.debug("REST request to get Crypto : {}", id);
        Optional<Crypto> crypto = cryptoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(crypto);
    }

    /**
     * {@code DELETE  /cryptos/:id} : delete the "id" crypto.
     *
     * @param id the id of the crypto to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cryptos/{id}")
    public ResponseEntity<Void> deleteCrypto(@PathVariable Long id) {
        log.debug("REST request to delete Crypto : {}", id);
        cryptoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
