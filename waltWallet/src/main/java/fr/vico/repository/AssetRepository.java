package fr.vico.repository;

import fr.vico.domain.Asset;

import fr.vico.domain.Crypto;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Asset entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssetRepository extends JpaRepository<Asset, Long> {

    @Query("select a from Asset a join Crypto c on a.crypto = c.id where c.symbol = ?1")
    List<Asset> getAssetsBySymbol(String symbol);
}
