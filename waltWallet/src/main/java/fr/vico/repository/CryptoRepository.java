package fr.vico.repository;

import fr.vico.domain.Crypto;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Crypto entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CryptoRepository extends JpaRepository<Crypto, Long> {
    /**
     * This method is created by JPA from its name.
     * @param symbol
     * @return Crypto with the symbol, null instead
     */
    Crypto getBySymbol(String symbol);

}
